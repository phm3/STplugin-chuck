import sublime, sublime_plugin
import datetime
import requests
import urllib, json

#this is my first attempt to write a plug for sublime text
# based on the hello world example



class ChuckCommand(sublime_plugin.TextCommand):

    def on_done(self, index):

        #  if user cancels with Esc key, do nothing
        #  if canceled, index is returned as  -1
        if index == -1:
            return

        # if user picks from list, return the correct entry
        self.view.run_command(
            "insert_my_text", {"args":
            {'text': self.list[index]}})

    def run(self, edit):

    	url = "http://api.icndb.com/jokes/random"
    	r = urllib.request.urlopen(url)
    	data = json.loads(r.read().decode(r.info().get_param('charset') or 'utf-8'))
    	
    	self.list = [str(data["value"]["joke"]).replace("&quot;", "\"")]

    	self.view.window().show_quick_panel(self.list, self.on_done,
            1, 2)


class InsertMyText(sublime_plugin.TextCommand):

    def run(self, edit, args):

        # add this to insert at current cursor position
        # http://www.sublimetext.com/forum/viewtopic.php?f=6&t=11509

        self.view.insert(edit, self.view.sel()[0].begin(), args['text'])


